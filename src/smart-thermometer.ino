#include <spark-dallas-temperature.h>
#include <OneWire.h>

OneWire oneWire(D4);
DallasTemperature sensors(&oneWire);
int temp;

void setup(void)
{
  Serial.begin(9600);
  sensors.begin();
  
  Particle.variable("Temperature", temp);
  Particle.function("publishTemp", publishTemp);
  
  Serial.begin(9600);
}

int publishTemp(String args){
    Particle.publish("Temperature", "" + String(temp), 21600, PRIVATE);
}


void loop(void) {
  sensors.requestTemperatures(); 
  temp =  int(round(sensors.getTempFByIndex(0)));
  Serial.print("Celsius temperature: ");
  Serial.print(sensors.getTempCByIndex(0)); 
  Serial.print(" - Fahrenheit temperature: ");
  Serial.println(sensors.getTempFByIndex(0));
  delay(1000);
}
