# Smart Homebrew Thermometer
A smart thermometer developed to integrate with Google Home and monitor your fermenting beer's temperature. This project uses a Particle Argon and IFTTT as the main pieces to the puzzle.

## Components - HW & SW
- **Particle Argon:** The main brain of the operation. Reads from the DS18B20 waterproof temp sensors and publishes events with those values to the Particle Cloud. Programmed with `src/smart-thermometer.ino `
- **Particle Cloud:** Communicates with the Particle Argon. Gets the temperature sensor and holds that value to be available for IFTTT to read.
- **IFTTT:** The glue to connect Particle Cloud, Google Assistant and Android notifications.
- **Google Assistant:** Initiates and holds routine to trigger the retrieval of the fermenter's temperature.

## Complications
- IFTTT does not allow for Google Assistant to be the 'THAT' reaction, so a small workaround had to be found. I used the IFTTT app on my phone to push a notification to me with the temperature instead of Google Assistant reading it back.
A solution would be to make a Google Application through Google Developer with a Firebase backend, and is the current working process for getting the application up to my original idea.

## Future Ideas
- Add a warning for temperatures that get too high or low, pushed to an android device.
